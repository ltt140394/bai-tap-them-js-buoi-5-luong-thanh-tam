// Bài tập 1
function tinhThue() {
    var hoTen = document.getElementById("txtUserName").value;
    var tongThuNhap = document.getElementById("txtTongThuNhap").value * 1;
    var soNguoiPhuThuoc = document.getElementById("txtNguoiPhuThuoc").value * 1;

    var thuNhapChiuThue = tongThuNhap - 4e+6 - soNguoiPhuThuoc * 1.6e+6;

    var thueTNCN;
    var thueSuatThuNhapDuoi60tr = 0.05;
    var thueSuatThuNhap60tr_120tr = 0.1;
    var thueSuatThuNhap120tr_210tr = 0.15;
    var thueSuatThuNhap210tr_384tr = 0.2;
    var thueSuatThuNhap384tr_624tr = 0.25;
    var thueSuatThuNhap624tr_960tr = 0.3;
    var thueSuatThuNhapTren960tr = 0.35;

    if (thuNhapChiuThue <= 0) {
        alert("Tổng thu nhập không chính xác");
        thueTNCNFormat = 0;
    } else if (thuNhapChiuThue <= 60e+6) {
        thueTNCN = thuNhapChiuThue * thueSuatThuNhapDuoi60tr;
    } else if (thuNhapChiuThue <= 120e+6) {
        thueTNCN = 60e+6 * thueSuatThuNhapDuoi60tr + (thuNhapChiuThue - 60e+6) * thueSuatThuNhap60tr_120tr;
    } else if (thuNhapChiuThue <= 210e+6) {
        thueTNCN = 60e+6 * thueSuatThuNhapDuoi60tr + 60e+6 * thueSuatThuNhap60tr_120tr + (thuNhapChiuThue - 120e+6) * thueSuatThuNhap120tr_210tr;
    } else if (thuNhapChiuThue <= 384e+6) {
        thueTNCN = 60e+6 * thueSuatThuNhapDuoi60tr + 60e+6 * thueSuatThuNhap60tr_120tr + 90e+6 * thueSuatThuNhap120tr_210tr + (thuNhapChiuThue - 210e+6) * thueSuatThuNhap210tr_384tr;
    } else if (thuNhapChiuThue <= 624e+6) {
        thueTNCN = 60e+6 * thueSuatThuNhapDuoi60tr + 60e+6 * thueSuatThuNhap60tr_120tr + 90e+6 * thueSuatThuNhap120tr_210tr + 174e+6 * thueSuatThuNhap210tr_384tr + (thuNhapChiuThue - 384e+6) * thueSuatThuNhap384tr_624tr;
    } else if (thuNhapChiuThue <= 960e+6) {
        thueTNCN = 60e+6 * thueSuatThuNhapDuoi60tr + 60e+6 * thueSuatThuNhap60tr_120tr + 90e+6 * thueSuatThuNhap120tr_210tr + 174e+6 * thueSuatThuNhap210tr_384tr + 240e+6 * thueSuatThuNhap384tr_624tr + (thuNhapChiuThue - 624e+6) * thueSuatThuNhap624tr_960tr;
    } else {
        thueTNCN = 60e+6 * thueSuatThuNhapDuoi60tr + 60e+6 * thueSuatThuNhap60tr_120tr + 90e+6 * thueSuatThuNhap120tr_210tr + 174e+6 * thueSuatThuNhap210tr_384tr + 240e+6 * thueSuatThuNhap384tr_624tr + 336e+6 * thueSuatThuNhap624tr_960tr + (thuNhapChiuThue - 960e+6) * thueSuatThuNhapTren960tr;
    }

    var thueTNCNFormat = new Intl.NumberFormat('vn-VN').format(thueTNCN);

    document.getElementById("show-ket-qua-bt1").style.display = "block";
    document.getElementById("show-ket-qua-bt1").innerHTML = `
    <p>Họ và tên : ${hoTen}</p>
    <p>Tiền thuế TNCN phải đóng : ${thueTNCNFormat} VND</p>
    `
}


// Bài tập 2
function hienDivKetNoi() {
    var loaiKH = document.getElementById("selectLoaiKhachHang").value;
    if (loaiKH == "doanhNghiep") {
        document.getElementById("divKetNoi").style.display = "block";
    } else {
        document.getElementById("divKetNoi").style.display = "none";
    }
}

function tinhTienCap() {
    var maKH = document.getElementById("txtMaKhachHang").value;
    var loaiKH = document.getElementById("selectLoaiKhachHang").value;
    var soKenhCaoCap = document.getElementById("txtSoKenh").value * 1;
    var soKetNoi = document.getElementById("txtSoKetNoi").value * 1;

    var tienPhaiTra;

    var phiXuLyHoaDonNhaDan = 4.5;
    var phiDichVuNhaDan = 20.5;
    var phiThueKenhNhaDan = 7.5;

    var phiXuLyHoaDonDoanhNghiep = 15;
    var phiDichVuDoanhNghiep10KetNoiDau = 75;
    var phiDichVuDoanhNghiepMoiKetNoiSau10KetNoi = 5;
    var phiThueKenhDoanhNghiep = 50;

    if (loaiKH == "nhaDan") {
        tienPhaiTra = phiXuLyHoaDonNhaDan + phiDichVuNhaDan + phiThueKenhNhaDan * soKenhCaoCap;
    } else if (loaiKH == "doanhNghiep") {
        if (soKetNoi <= 10) {
            tienPhaiTra = phiXuLyHoaDonDoanhNghiep + phiDichVuDoanhNghiep10KetNoiDau + phiThueKenhDoanhNghiep * soKenhCaoCap;
        } else {
            tienPhaiTra = phiXuLyHoaDonDoanhNghiep + phiDichVuDoanhNghiep10KetNoiDau + (soKetNoi - 10) * phiDichVuDoanhNghiepMoiKetNoiSau10KetNoi + phiThueKenhDoanhNghiep * soKenhCaoCap;
        }
    } else {
        tienPhaiTraFormat = 0;
        alert("Phải chọn loại khách hàng")
    }

    var tienPhaiTraFormat = new Intl.NumberFormat('en-US', {
        style: "currency",
        currency: "USD",
    }).format(tienPhaiTra);

    document.getElementById("show-ket-qua-bt2").style.display = "block";
    document.getElementById("show-ket-qua-bt2").innerHTML = `
    <p>Mã khách hàng : ${maKH}</p>
    <p>Số tiền cáp phải đóng : ${tienPhaiTraFormat}</p>
    `;

}